package net.welovespigotplugins.worldmanager.objects;

import com.google.common.collect.Maps;
import net.welovespigotplugins.worldmanager.utils.ConfigUtils;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import java.io.File;
import java.io.IOException;
import java.util.Map;

/**
 * JavaDoc this file!
 * Created: 04.10.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class WorldContainer {

    private static final Map<String, WorldContainer> WORLD_CONTAINER_MAP = Maps.newConcurrentMap();
    private final String name;
    private Location spawnLocation;

    public WorldContainer(final String worldName) {
        this.name = worldName;
        WORLD_CONTAINER_MAP.put(worldName, this);
    }

    public String getName() {
        return name;
    }

    public Location getSpawnLocation() {
        return this.spawnLocation;
    }

    public void setSpawnLocation(final Location location) {
        this.spawnLocation = location;
        new ConfigUtils().saveConfig();
    }

    public static Map<String, WorldContainer> getWorldContainerMap() {
        return WORLD_CONTAINER_MAP;
    }

    public void loadWorld() {
        Bukkit.createWorld(new WorldCreator(this.name));
    }

    public void deleteWorld() {

        Bukkit.unloadWorld(this.name, true);

        try {
            FileUtils.deleteDirectory(new File(System.getProperty("user.dir") + "/" + this.name));
        } catch (IOException e) {
            e.printStackTrace();
        }

        getWorldContainerMap().remove(this.name);
        new ConfigUtils().saveConfig();

    }

    public void createWorld(final World.Environment environment) {
        Bukkit.createWorld(new WorldCreator(this.name).environment(environment));
    }

}
