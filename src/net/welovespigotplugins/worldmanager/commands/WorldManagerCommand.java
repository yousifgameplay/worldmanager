package net.welovespigotplugins.worldmanager.commands;

import net.welovespigotplugins.worldmanager.WorldManager;
import net.welovespigotplugins.worldmanager.objects.WorldContainer;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;

/**
 * JavaDoc this file!
 * Created: 04.10.2018
 *
 * @author WeLoveSpigotPlugins (welovespigotplugins@gmail.com)
 */
public class WorldManagerCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        if (args.length == 0) {

            if (!commandSender.hasPermission("wm.description")) {
                sendNoPermissionMessage(commandSender);
                return true;
            }

            sendDescription(commandSender);

            return true;
        }

        switch (args.length) {

            case 1:

                switch (args[0].toUpperCase()) {
                    case "LIST":

                        if (!commandSender.hasPermission("wm.list")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        for (String worldContainer : WorldContainer.getWorldContainerMap().keySet()) {
                            commandSender.sendMessage(WorldManager.PREFIX + "§8» §b" + worldContainer);
                        }

                        return true;

                    default:
                        sendDescription(commandSender);
                        return true;
                }

            case 2:

                final String worldName = args[1];

                switch (args[0].toUpperCase()) {

                    case "DELETE":

                        if (!commandSender.hasPermission("wm.delete")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        if (WorldContainer.getWorldContainerMap().containsKey(worldName)) {

                            for(Player all : Bukkit.getOnlinePlayers()) {
                                if(all.getWorld().getName().equals(worldName)) {
                                    commandSender.sendMessage(WorldManager.PREFIX + "§7Could not delete world §b" + worldName + "§7 because players are playing on this map§8!");
                                    return true;
                                }
                            }

                            WorldContainer.getWorldContainerMap().get(worldName).deleteWorld();
                            commandSender.sendMessage(WorldManager.PREFIX + "§7World §b" + worldName + "§7 successfully deleted§8!");
                            return true;

                        } else {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7This world does not exists§8!");
                            return true;
                        }


                    case "TELEPORT":

                        if (!commandSender.hasPermission("wm.teleport")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        if (WorldContainer.getWorldContainerMap().containsKey(worldName)) {

                            final Player player = (Player) commandSender;

                            if(WorldContainer.getWorldContainerMap().get(worldName).getSpawnLocation() != null) {

                                player.teleport(WorldContainer.getWorldContainerMap().get(worldName).getSpawnLocation());

                            } else {

                                player.teleport(Bukkit.getWorld(worldName).getSpawnLocation());
                                return true;

                            }

                            player.sendMessage(WorldManager.PREFIX + "§7Successfully teleported to the world §b" + worldName);
                            return true;

                        } else {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7A world called §b" + worldName + "§7 could not be found§8!");
                            return true;
                        }

                    case "RESETSPAWNLOCATION":

                        if (!commandSender.hasPermission("wm.resetspawnlocation")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        if (WorldContainer.getWorldContainerMap().containsKey(worldName)) {

                            WorldContainer.getWorldContainerMap().get(worldName).setSpawnLocation(null);
                            commandSender.sendMessage(WorldManager.PREFIX + "§7The worldspawn of §b" + worldName + "§7 was deleted successfully§8!");
                            return true;

                        } else {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7A world called §b" + worldName + "§7 could not be found§8!");
                            return true;
                        }

                    case "SETSPAWNLOCATION":

                        if (!commandSender.hasPermission("wm.setspawnlocation")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        if(WorldContainer.getWorldContainerMap().containsKey(worldName)) {

                            final WorldContainer worldContainer = WorldContainer.getWorldContainerMap().get(worldName);
                            final Player player = (Player) commandSender;
                            worldContainer.setSpawnLocation(player.getLocation());
                            player.sendMessage(WorldManager.PREFIX + "§7The §bSpawnlocation §7was changed successfully on world §b" + worldName + "§8!");
                            return true;

                        } else {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7There isn't a world type called §b" + worldName);
                            return true;
                        }

                    case "IMPORT":

                        if (!commandSender.hasPermission("wm.import")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        if(WorldContainer.getWorldContainerMap().containsKey(args[1])) {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7The world §b" + args[1] + "§7 is already known§8!");
                            return true;
                        }

                        if(!new File(System.getProperty("user.dir") + "/" + args[1]).exists()) {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7The map §b" + args[1] + "§7 does not exist§8!");
                            return true;
                        }

                        commandSender.sendMessage(WorldManager.PREFIX + "§7Starting importing map §b" + args[1] + "§8!");
                        Bukkit.createWorld(new WorldCreator(args[1]));

                        commandSender.sendMessage(WorldManager.PREFIX + "§7Successfully imported §b" + args[1] + "§8!");

                        final WorldContainer worldContainer = new WorldContainer(args[1]);
                        WorldContainer.getWorldContainerMap().put(args[1], worldContainer);
                        worldContainer.setSpawnLocation(Bukkit.getWorld(args[1]).getSpawnLocation());

                        return true;

                    case "UNLOAD":

                        if (!commandSender.hasPermission("wm.unload")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        if(!WorldContainer.getWorldContainerMap().containsKey(args[1])) {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7The map §b" + args[1] + "§7 is not loaded§8!");
                            return true;
                        }

                        commandSender.sendMessage(WorldManager.PREFIX + "§7Starting unloading of §b" + args[1] + "§8!");
                        Bukkit.unloadWorld(args[1], true);
                        WorldContainer.getWorldContainerMap().remove(args[1]);

                        commandSender.sendMessage(WorldManager.PREFIX + "§7Successfully unloaded §b" + args[1] + "§8!");
                        return true;

                }



            case 3:

                final String name = args[1];



                switch (args[0].toUpperCase()) {

                    case "CREATE":

                        if (!commandSender.hasPermission("wm.createworld")) {
                            sendNoPermissionMessage(commandSender);
                            return true;
                        }

                        final String type = args[2];

                        if (!WorldContainer.getWorldContainerMap().containsKey(name)) {

                            if (type.equalsIgnoreCase("normal") || type.equalsIgnoreCase("nether") || type.equalsIgnoreCase("end")) {

                                final WorldContainer worldContainer = new WorldContainer(name);
                                WorldContainer.getWorldContainerMap().put(name, worldContainer);

                                switch (type.toUpperCase()) {
                                    case "NORMAL":
                                        commandSender.sendMessage(WorldManager.PREFIX + "§7Please wait...");
                                        worldContainer.createWorld(World.Environment.NORMAL);
                                        commandSender.sendMessage(WorldManager.PREFIX + "§aFinished!");
                                        break;
                                    case "NETHER":
                                        commandSender.sendMessage(WorldManager.PREFIX + "§7Please wait...");
                                        worldContainer.createWorld(World.Environment.NETHER);
                                        commandSender.sendMessage(WorldManager.PREFIX + "§aFinished!");
                                        break;
                                    case "END":
                                        commandSender.sendMessage(WorldManager.PREFIX + "§7Please wait...");
                                        worldContainer.createWorld(World.Environment.THE_END);
                                        commandSender.sendMessage(WorldManager.PREFIX + "§aFinished!");
                                        break;
                                    default:
                                        sendDescription(commandSender);
                                        return true;
                                }

                            } else {

                                commandSender.sendMessage(WorldManager.PREFIX + "§7There isn't a world type called §b" + type);
                                return true;
                            }

                        } else {
                            commandSender.sendMessage(WorldManager.PREFIX + "§7There already exists a map called §b" + name + "§8!");
                            return true;
                        }

                        return true;

                }


        }

        return false;
    }

    private void sendNoPermissionMessage(final CommandSender commandSender) {
        commandSender.sendMessage(WorldManager.PREFIX + "§7");
    }

    private void sendDescription(final CommandSender commandSender) {
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bCreate §8[§bName§8] §8[§7Normal§8, §cNether§8, §eEnd§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bDelete §8[§bName§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bImport §8[§bName§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bUnload §8[§bName§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bSetSpawnLocation §8[§bName§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bResetSpawnLocation §8[§bName§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bTeleport §8[§bName§8]");
        commandSender.sendMessage(WorldManager.PREFIX + "§7/WM §bList");
    }

}
